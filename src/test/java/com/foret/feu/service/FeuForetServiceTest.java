package com.foret.feu.service;

import com.foret.feu.config.FeuForetConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class FeuForetServiceTest {
    @Mock
    FeuForetConfig feuForetConfig;
    @InjectMocks
    FeuForetService feuForetService;

    @Test
    void flaggerLesDernieresCasesBrulees_CasNominal() {
        //GIVEN
        int[][] foret = {{0, 1, 0}, {0, 1, 0}, {1, 0, 2}};
        int hauteur = 3;
        int largeur = 3;
        int[][] foretExpected = {{0, -1, 0}, {0, -1, 0}, {-1, 0, 2}};


        //WHEN
        int[][] foretActual = feuForetService.flaggerLesDernieresCasesBrulees(foret, hauteur, largeur);

        //THEN
        assertArrayEquals(foretExpected, foretActual);

    }

    @Test
    void flaggerLesDernieresCasesBrulees_LorsqueTouTesLesCasesSontA0() {
        //GIVEN
        int[][] foret = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
        int hauteur = 3;
        int largeur = 3;
        int[][] foretExpected = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};


        //WHEN
        int[][] foretActual = feuForetService.flaggerLesDernieresCasesBrulees(foret, hauteur, largeur);

        //THEN
        assertArrayEquals(foretExpected, foretActual);

    }

    @Test
    void isCaseEnFeu_PasDeCaseEnFeu(){
        //GIVEN
        int[][] foret = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
        int hauteur = 3;
        int largeur = 3;


        //WHEN THEN
        assertFalse(feuForetService.isCaseEnFeu(foret, hauteur, largeur));
    }

    @Test
    void isCaseEnFeu_AvecUneSeuleCaseEnFeu(){
        //GIVEN
        int[][] foret = {{1, 0, 0}, {0, 0, 0}, {0, 0, 0}};
        int hauteur = 3;
        int largeur = 3;


        //WHEN THEN
        assertTrue(feuForetService.isCaseEnFeu(foret, hauteur, largeur));
    }

    @Test
    void isCaseEnFeu_AvecPlusiersCaseEnFeu(){
        //GIVEN
        int[][] foret = {{1, 0, 0}, {0, 1, 1}, {1, 1, 0}};
        int hauteur = 3;
        int largeur = 3;


        //WHEN THEN
        assertTrue(feuForetService.isCaseEnFeu(foret, hauteur, largeur));
    }

}