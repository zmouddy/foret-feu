package com.foret.feu.config;

import com.foret.feu.model.Point;
import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "foret")
@Data
@ToString
public class FeuForetConfig {
    int hauteur;
    int largeur;
    double probabilite;
    List<Point> casesInitiales;
}
