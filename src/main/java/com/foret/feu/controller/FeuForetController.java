package com.foret.feu.controller;

import com.foret.feu.config.FeuForetConfig;
import com.foret.feu.model.Point;
import com.foret.feu.model.ReponseSimulationFeuForet;
import com.foret.feu.service.FeuForetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/foret/feu")
public class FeuForetController {

    @Autowired
    FeuForetService feuForetService;
    @Autowired
    FeuForetConfig feuForetConfig;

    @RequestMapping(value = "demarreSimulation", method = RequestMethod.POST)
    public ReponseSimulationFeuForet demarrerSimulationFeuForet(){
        int[][] foretInitialeAvecFeux = this.declenchementDesFeuxDeForet();
        return feuForetService.demarrerSimulationFeuForet(foretInitialeAvecFeux);
    }
    private int[][] declenchementDesFeuxDeForet() {
        int hauteur = feuForetConfig.getHauteur();
        int largeur = feuForetConfig.getLargeur();
        int[][] grille = new int[hauteur][largeur];
        for (int x = 0; x < hauteur; x++) {
            for (int y = 0; y < largeur; y++) {
                grille[x][y]=0;
            }
        }
        for (Point caseInitial : feuForetConfig.getCasesInitiales()) {
            grille[caseInitial.getX()][caseInitial.getY()]=1;
        }

        return grille;
    }
}
