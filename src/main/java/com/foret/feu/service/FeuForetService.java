package com.foret.feu.service;

import com.foret.feu.config.FeuForetConfig;
import com.foret.feu.model.ReponseSimulationFeuForet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class FeuForetService {

    @Autowired
    FeuForetConfig feuForetConfig;

    //0 : case vide
    //1 : case en feu
    //-1: case déjà brûlée
    //2 : case en cendre
    public ReponseSimulationFeuForet demarrerSimulationFeuForet(int[][] foret) {
        Random probabiliteDeclenchementFeu = new Random();
        int hauteur = feuForetConfig.getHauteur();
        int largeur = feuForetConfig.getLargeur();
        double probabilite = feuForetConfig.getProbabilite();
        int nombreDeCaseEnCendre = 0;
        int nombreEtapesEcoulees = 0;

        affichageDeLaForet(foret, hauteur, largeur, nombreEtapesEcoulees);

        while (isCaseEnFeu(foret, hauteur, largeur)) {
            nombreEtapesEcoulees++;

            foret = flaggerLesDernieresCasesBrulees(foret, hauteur, largeur);

            for (int x = 0; x < hauteur; x++) {
                for (int y = 0; y < largeur; y++) {
                    if (foret[x][y] == -1) {
                        foret[x][y] = 2;  // la case en feu devient une cendre
                        nombreDeCaseEnCendre++;
                        // propagation aux cases adjacentes
                        if (x > 0 && foret[x - 1][y] != 2 && probabiliteDeclenchementFeu.nextDouble() < probabilite) {
                            foret[x - 1][y] = 1;
                        }
                        if (x < hauteur - 1 && foret[x + 1][y] != 2 && probabiliteDeclenchementFeu.nextDouble() < probabilite) {
                            foret[x + 1][y] = 1;
                        }
                        if (y > 0 && foret[x][y - 1] != 2 && probabiliteDeclenchementFeu.nextDouble() < probabilite) {
                            foret[x][y - 1] = 1;
                        }
                        if (y < largeur - 1 && foret[x][y + 1] != 2 && probabiliteDeclenchementFeu.nextDouble() < probabilite) {
                            foret[x][y + 1] = 1;
                        }
                    }
                }
            }
            affichageDeLaForet(foret, hauteur, largeur, nombreEtapesEcoulees);
        }
        return new ReponseSimulationFeuForet(nombreDeCaseEnCendre, nombreEtapesEcoulees);
    }

    protected int[][]  flaggerLesDernieresCasesBrulees(int[][] grille, int hauteur, int largeur) {
        for (int x = 0; x < hauteur; x++) {
            for (int y = 0; y < largeur; y++) {
                if (grille[x][y] == 1) grille[x][y] = -1;
            }
        }
        return grille;
    }

    private void affichageDeLaForet(int[][] grille, int hauteur, int largeur, int nombreEtapesEcoulees) {
        System.out.println("********** Etape :  " + nombreEtapesEcoulees);

        for (int x = 0; x < hauteur; x++) {
            for (int y = 0; y < largeur; y++) {

                System.out.print(grille[x][y]);
            }
            System.out.println();
        }

        System.out.println();
        System.out.println();
        System.out.println();
    }

    protected boolean isCaseEnFeu(int[][] grille, int hauteur, int largeur) {
        for (int x = 0; x < hauteur; x++) {
            for (int y = 0; y < largeur; y++) {
                if (grille[x][y] == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
