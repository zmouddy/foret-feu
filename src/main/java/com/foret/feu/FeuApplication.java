package com.foret.feu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = "classpath:application.properties")
public class FeuApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeuApplication.class, args);
    }

}
